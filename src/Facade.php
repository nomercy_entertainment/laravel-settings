<?php

namespace NoMercy\Settings;

class Facade extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritdoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'NoMercy\Settings\Setting\SettingStorage';
    }
}
